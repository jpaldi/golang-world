run:
	docker-compose build && docker-compose up

test:
	docker-compose -f docker-compose-test.yml build && docker-compose -f docker-compose-test.yml up -d
	go test ./...
