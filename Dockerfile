FROM golang:1.8

WORKDIR /app

COPY . .

RUN go build ./cmd/...

CMD ["./joao-tool"]
