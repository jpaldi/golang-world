package models

import (
	"os"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestDatabase(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Entry Suite")
}

var _ = Describe("Db", func() {

	var (
		host       string
		database   string
		collection string
		user       string
		pw         string
		e1         Entry
	)

	BeforeEach(func() {
		host = os.Getenv("HOST")
		database = os.Getenv("DATABASE")
		collection = os.Getenv("COLLECTION")
		user = os.Getenv("USER")
		pw = os.Getenv("PASS")
		e1 = Entry{
			SensorID: "joao-sensor",
			Measures: []EntryMeasure{
				{
					Name: "temperature",
					Data: []Data{
						{1, 20.9},
						{2, 21.2},
					},
				},
				{
					Name: "humidity",
					Data: []Data{
						{1, 50.2},
						{2, 49.8},
					},
				},
			},
		}
	})

	Describe("Database", func() {
		Context("Should connect to the database", func() {

			It("add entry to db", func() {
				//TO DO - figure it out why docker-compose build/up don't set env variables
				host = "localhost:27017"
				database = "sensorstest"
				collection = "sensors"

				Expect(host).To(Equal("localhost:27017"))
				addDBEntry(host, database, collection, e1)

				entry := findDBEntry(host, database, collection, "joao-sensor")
				Expect(entry.SensorID).To(Equal("joao-sensor"))

				temperature := getMeasureData(e1, "temperature")
				Expect(temperature[0].Value).To(Equal(float32(20.9)))

				removeDBEntry(host, database, collection, "joao-sensor")
			})
		})
	})
})
