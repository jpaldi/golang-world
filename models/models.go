package models

// Entry : represents a JSON asset entry where ID represents asset ID
type Entry struct {
	SensorID string         `json:"sensorid"`
	Measures []EntryMeasure `json:"measures"`
}

// EntryMeasure : represents a measure and an array of data for that measure
type EntryMeasure struct {
	Name string `json:"measure-name"`
	Data []Data `json:"measure-data"`
}

// Data : represents a timestamp value mapping
type Data struct {
	Timestamp int     `json:"timestamp"`
	Value     float32 `json:"value"`
}

func getMeasures(e Entry) []string {
	var measuresList []string
	for _, measure := range e.Measures {
		measuresList = append(measuresList, measure.Name)
	}
	return measuresList
}

func getMeasureData(e Entry, name string) []Data {
	for _, measure := range e.Measures {
		if measure.Name == name {
			return measure.Data
		}
	}
	return nil
}

func createData(t int, v float32) Data {
	obj := Data{}
	obj.Value = v
	obj.Timestamp = t

	return obj
}

func createEntryMeasure(name string) EntryMeasure {
	obj := EntryMeasure{}
	obj.Name = name

	return obj
}

func createEntry(ID string) Entry {
	obj := Entry{}
	obj.SensorID = ID

	return obj
}
