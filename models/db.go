package models

import (
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func addDBEntry(host string, db string, coll string, e Entry) {
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:   []string{host},
		Timeout: 5 * time.Second,
	}

	mongoSession, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		panic(err)
	}

	c := mongoSession.DB(db).C(coll)

	entry := findDBEntry(host, db, coll, e.SensorID)

	if entry.SensorID != "" {
		panic("sensorAlreadyExists")
	}
	c.Insert(e)

}

func findDBEntry(host string, db string, coll string, sensorid string) Entry {
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:   []string{host},
		Timeout: 5 * time.Second,
	}

	mongoSession, err := mgo.DialWithInfo(mongoDBDialInfo)

	if err != nil {
		panic(err)
	}

	c := mongoSession.DB(db).C(coll)

	// Query One
	var result Entry
	c.Find(bson.M{"sensorid": sensorid}).One(&result)

	return result
}

func removeDBEntry(host string, db string, coll string, sensorid string) {
	mongoDBDialInfo := &mgo.DialInfo{
		Addrs:   []string{host},
		Timeout: 5 * time.Second,
	}

	mongoSession, err := mgo.DialWithInfo(mongoDBDialInfo)
	if err != nil {
		panic(err)
	}

	c := mongoSession.DB(db).C(coll)

	c.Remove(bson.M{"sensorid": sensorid})
	if err != nil {
		panic(err)
	}
}
