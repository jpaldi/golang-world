package models

import (
	"reflect"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestEntries(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Entry Suite")
}

var _ = Describe("Entry", func() {

	var (
		e1 Entry
	)

	BeforeEach(func() {
		e1 = Entry{
			SensorID: "joao-sensor",
			Measures: []EntryMeasure{
				{
					Name: "temperature",
					Data: []Data{
						{1, 20.9},
						{2, 21.2},
					},
				},
				{
					Name: "humidity",
					Data: []Data{
						{1, 50.2},
						{2, 49.8},
					},
				},
			},
		}
	})

	Describe("Check test sensor properties", func() {
		Context("Check id", func() {
			It("name should be joao-sensor", func() {
				Expect(e1.SensorID).To(Equal("joao-sensor"))
			})
			It("getMeasures should return 2 measures", func() {
				measures := getMeasures(e1)
				Expect(measures[0]).To(Equal("temperature"))
				Expect(measures[1]).To(Equal("humidity"))
			})
			It("getData for temperature should 2 data entries", func() {
				temperature := getMeasureData(e1, "temperature")
				Expect(temperature[0].Value).To(Equal(float32(20.9)))
			})
			It("getData for disturbance should return an empty list", func() {
				disturbance := getMeasureData(e1, "disturbance")
				Expect(len(disturbance)).To(Equal(0))
			})
			It("create new Data object", func() {
				obj := createData(1, 1.1)
				Expect(reflect.TypeOf(obj).Name()).To(Equal("Data"))
				Expect(obj.Value).To(Equal(float32(1.1)))
				Expect(obj.Timestamp).To(Equal(1))
			})
			It("create new EntryMeasure object", func() {
				obj := createEntryMeasure("disturbance")
				Expect(reflect.TypeOf(obj).Name()).To(Equal("EntryMeasure"))
				Expect(obj.Name).To(Equal("disturbance"))
				Expect(len(obj.Data)).To(Equal(0))
			})
			It("create new Entry object", func() {
				obj := createEntry("sensor1")
				Expect(reflect.TypeOf(obj).Name()).To(Equal("Entry"))
				Expect(obj.SensorID).To(Equal("sensor1"))
				Expect(len(obj.Measures)).To(Equal(0))
			})
		})
	})
})
