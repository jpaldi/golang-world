package main

import (
	"fmt"
	"os"
)

func main() {
	fmt.Println("Hello my name is: " + os.Getenv("NAME"))
}
